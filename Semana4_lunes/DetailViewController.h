//
//  DetailViewController.h
//  Semana4_lunes
//
//  Created by Rafa Paradela on 25/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "TVshow.h"

@interface DetailViewController : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *myTitle;
@property (weak, nonatomic) IBOutlet UITextView *myDescription;

@property (nonatomic, strong) TVshow * serie;

@end
