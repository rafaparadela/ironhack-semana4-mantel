//
//  UserEntity.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 24/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "UserEntity.h"


@implementation UserEntity

@dynamic userName;
@dynamic userId;
@dynamic userPassword;

@end
