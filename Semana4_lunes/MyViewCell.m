//
//  MyViewCell.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "MyViewCell.h"
#import "MyTableViewController.h"

@interface MyViewCell ()

@property (strong, nonatomic) MyViewCell * seleccionado;

@end

@implementation MyViewCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib
{
    // Initialization code
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)goLike:(id)sender {

    self.seleccionado = (MyViewCell *) sender;
    

    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Le has dado" message:@"Gachón" delegate:self cancelButtonTitle:@"No" otherButtonTitles:nil];
    [alert addButtonWithTitle:@"Si"];
    [alert show];
    [self addLike: self.seleccionado];
    
    
}


- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    NSLog(@"Button Index =%d",buttonIndex);
    if(buttonIndex == 1)
    {
        NSLog(@"You have clicked GOO");
        [self addLike: self.seleccionado];
    }
}



- (void) addLike:(id)sender{
    MyViewCell * celda = (MyViewCell *) sender;
    NSString * serieId = [NSString stringWithFormat:@"%d",celda.tag];
    NSLog(@"Tag: %@", serieId);
    
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    NSString *filePath = [documentsPath stringByAppendingPathComponent: [NSString stringWithFormat:@"%@_likes.plist",self.loggedUser.userId]];
    NSDictionary *plisData = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    if(plisData){
        NSMutableArray * likes = [NSMutableArray arrayWithArray:[plisData objectForKey:@"likes"] ];
        if (![likes containsObject:serieId]){
            [likes addObject:serieId];
            NSLog(@"entro1");
            
        }else{
            [likes removeObject:serieId];
                        NSLog(@"entro2");
        }
        [self addLikeInArray:likes toPlist:filePath];
    }
    else{
                    NSLog(@"entro3");
        NSArray * uno = [[NSArray alloc] initWithObjects:serieId, nil];
        [self addLikeInArray:uno toPlist:filePath];
    }
    
    
    
    
}

- (void) addLikeInArray: (NSArray *) lista toPlist: (NSString *) filePath{
    NSDictionary * dictionary = [[NSDictionary alloc] initWithObjectsAndKeys:lista, @"likes", nil];
    [dictionary writeToFile:filePath atomically:YES];
    
}


@end
