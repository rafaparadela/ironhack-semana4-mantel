//
//  MyTableViewController.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "MyTableViewController.h"
#import "MyViewCell.h"
#import "TVshow.h"
#import "DetailViewController.h"


static NSString * const savedShowsFileName=@"shows";

@interface MyTableViewController ()

@property (nonatomic, strong) NSMutableArray * myshows;
@property (nonatomic, strong) NSString *filePath;
@property (nonatomic, strong) NSDictionary *plisData;
@property (nonatomic, strong) NSArray * likes;

@property (nonatomic, strong) TVshow * seleccionada;

@end

@implementation MyTableViewController

- (id)initWithCoder:(NSCoder *)aDecoder{
    
    self = [super initWithCoder:aDecoder];
    if (self) {
        
        
        
        

        
        _myshows = [NSMutableArray array];
        
        
        
        
        
        NSURL * jsonURL = [NSURL URLWithString:@"http://ironhack4thweek.s3.amazonaws.com/shows.json"];
        NSData * seriesData = [NSData dataWithContentsOfURL:jsonURL];
        NSError * error;
        NSDictionary * JSONDictionary = [NSJSONSerialization JSONObjectWithData:seriesData options:NSJSONReadingMutableContainers error:&error];
        
        for (NSDictionary * tvshowDictionary in [JSONDictionary valueForKey:@"shows"]) {
            NSError * parseError;
            TVshow * showItem = [MTLJSONAdapter modelOfClass:[TVshow class] fromJSONDictionary:tvshowDictionary error:&parseError];
            [_myshows addObject:showItem];
            
        }
        
        
    }
    return self;
}


- (void)viewWillAppear:(BOOL)animated{
    self.title = [NSString stringWithFormat:@"Series (%@)", self.loggedUser.userName];
    
    [self leeLikes];
    
    
}

- (void) leeLikes{
    NSString *documentsPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) firstObject];
    self.filePath = [documentsPath stringByAppendingPathComponent: [NSString stringWithFormat:@"%@_likes.plist",self.loggedUser.userId]];
    self.plisData = [NSDictionary dictionaryWithContentsOfFile:self.filePath];
    
    if(self.plisData){
        self.likes = [NSArray arrayWithArray:[self.plisData objectForKey:@"likes"] ];
        
        
        
        
        NSFileManager *fileManager = [NSFileManager defaultManager];
        NSDictionary *attributes = [fileManager attributesOfItemAtPath:self.filePath error:nil];
        
        [attributes enumerateKeysAndObjectsUsingBlock:^(id key, id obj, BOOL *stop) {
            
            
            if([obj isKindOfClass:[NSNumber class]]){
                //NSLog(@"%@ - %f", (NSString *) key, [obj floatValue]);
            }
            else{
                //NSLog(@"%@ - %@", (NSString *) key, [obj class]);
            }
                
//
        }];
        
        
        
        
    }
    else{
        self.likes = @[];
    }
    
}


- (IBAction)goLogout:(id)sender {
    
    [self.coreDataManager.managedObjectContext deleteObject:self.loggedUser];
    NSError *error;
    [self.coreDataManager.managedObjectContext save:&error];
    [self.navigationController popViewControllerAnimated:YES];
    
    
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    NSError *newerror = nil;
    if (![fileManager removeItemAtPath:self.filePath error:&newerror]) {
        NSLog(@"[Error] %@ (%@)", newerror, self.filePath);
    }
    
}



- (NSMutableArray *)myshows
{
    if (!_myshows) {
        _myshows = [NSMutableArray array];
    }
    return _myshows;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    //[self load];
//    self.title = @"Series";
//    _myshows = [NSMutableArray array];
}



- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Añadir elementos
- (IBAction)addRandom:(id)sender {
    [self.myshows addObject:[self randomShow]];
    
    [self.tableView reloadData];

}

- (IBAction)copyLast:(id)sender {
    
    if([self.myshows count] > 0){
        TVshow * copyshow = [[self.myshows lastObject] copy];
        [self.myshows addObject:copyshow];
        [self.tableView reloadData];
    }
    
    [self save];
    
}

- (TVshow *) randomShow{
    NSArray * series = @[@"Prison Break",@"Lost",@"The Sopranos",@"The Wire",@"Breaking Bad",@"House of Cards",@"True Detective",@"game of Thrones"];
    NSUInteger randomIndex = arc4random() % [series count];
    TVshow * show = [[TVshow alloc] init];
    show.title = [series objectAtIndex:randomIndex];
    show.description = @"Description caramba";
    return show;
}

- (NSString *) archivePath{
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES);
    NSString * documentsDirectory = [paths objectAtIndex:0];
    return  [documentsDirectory stringByAppendingPathComponent:savedShowsFileName];
    
}

- (void) save{
    
    if (self.myshows.count) {
        [NSKeyedArchiver archiveRootObject:self.myshows toFile:[self archivePath] ];
    }
}

- (void) load{
    NSArray *saved = [NSKeyedUnarchiver unarchiveObjectWithFile:[self archivePath]];
//
    if(saved.count){
        self.myshows = [saved mutableCopy];
    }
    
}




- (IBAction)compare:(id)sender {
    
    BOOL comp = NO;
    if([self.myshows count]>0){
        TVshow * first = [self.myshows firstObject];
        TVshow * last = [self.myshows lastObject];
        
        if([first isEqual: last]){
            comp = YES;
        }
    }
    
    
    NSLog(@"%d",comp);
}




#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.myshows count];
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MyViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"mycell" forIndexPath:indexPath];
    
    cell.loggedUser = self.loggedUser;
    cell.coreDataManager = self.coreDataManager;
    
    TVshow * serie = [self.myshows objectAtIndex:indexPath.row];
    cell.myTitle.text = serie.title;
    cell.myDescription.text = serie.description;
    [cell.myLike setTag:[serie.id integerValue]];
    
    
    if([self.likes containsObject:serie.id]){
        [cell.myLike setTitle:@"🌟" forState:UIControlStateNormal];
    }
    else{
        [cell.myLike setTitle:@"👍" forState:UIControlStateNormal];
    }
    
    
    
    
//    NSURL *theurl = [NSURL URLWithString:serie.posterURL];
//    NSData *data = [NSData dataWithContentsOfURL: theurl];
//    UIImage *downloaded = [UIImage imageWithData:data];
//    cell.myPoster.image = downloaded;
    
    return cell;
}
- (IBAction)updateTable:(id)sender {
//    [self leeLikes];
//    [self.tableView reloadData];
    [self update];
}

- (void) update{
    [self leeLikes];
    [self.tableView reloadData];
}


#pragma mark - acciones

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    self.seleccionada = [self.myshows objectAtIndex:indexPath.row];
    NSLog(@"%d - %@",indexPath.row, self.seleccionada.title);
    
    [self performSegueWithIdentifier:@"segueDetail" sender:self];
    
}


- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"segueDetail"]) {
        DetailViewController *vc = segue.destinationViewController;
        vc.serie = self.seleccionada;
        NSLog(@"%@", self.seleccionada.title);
        
    }
}


/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath
{
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
