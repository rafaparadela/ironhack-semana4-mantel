//
//  LoginViewController.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 24/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "LoginViewController.h"
#import "CoreDataManager.h"
#import "MyTableViewController.h"
#import "UserEntity.h"

@interface LoginViewController ()

@property (strong, nonatomic) CoreDataManager * coreDataManager;

@property (weak, nonatomic) IBOutlet UITextField *inputName;
@property (weak, nonatomic) IBOutlet UITextField *inputPassword;

@end

@implementation LoginViewController


- (id)initWithCoder:(NSCoder *)aDecoder
{
    self = [super initWithCoder:aDecoder];
    if (self) {
        _coreDataManager = [[CoreDataManager alloc] initWithModelName:@"Shows"];
        
    }
    return self;
}



- (void)viewDidLoad
{
    [super viewDidLoad];
    UserEntity * user = [self loggedUser];
    
    if(user){
        [self performSegueWithIdentifier:@"mysegue" sender:self];
    }
}



#pragma mark - Users

- (UserEntity *) loggedUser{
    UserEntity *loggedUser;
    NSFetchRequest *request = [[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([UserEntity class])];
    request.predicate = [NSPredicate predicateWithFormat:@"1=1"];
//    request.predicate = [NSPredicate predicateWithFormat:@"userId=%@",self.inputName.text];
    NSArray *users = [self.coreDataManager.managedObjectContext executeFetchRequest:request error:nil];
    if(users.count){
        loggedUser = [users firstObject];
        
        [self writeLoginDate];
        
    }
    return loggedUser;
    
}

- (void) addUser{
    UserEntity * user = [NSEntityDescription insertNewObjectForEntityForName:NSStringFromClass([UserEntity class])
                                                      inManagedObjectContext:self.coreDataManager.managedObjectContext];
    

    user.userId = self.inputName.text;
    user.userName = self.inputName.text;
    
    NSError * error;
    [self.coreDataManager.managedObjectContext save:&error];
    
    
    
    
    
}

- (void) writeLoginDate{
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd-MM-yyyy HH:mm"];
    NSString *dateString = [formatter stringFromDate:[NSDate date]];
    [[NSUserDefaults standardUserDefaults]setObject:dateString forKey:@"lastLogin"];
}

#pragma mark - actions
- (IBAction)goLogin:(id)sender {
    [self addUser];
}


#pragma mark - segue
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if([segue.identifier  isEqual: @"mysegue"]){
        MyTableViewController* vc = segue.destinationViewController;
        
        UserEntity * s = [self loggedUser];
        vc.loggedUser = s;
        vc.coreDataManager = self.coreDataManager;
        
    }
    
}


@end
