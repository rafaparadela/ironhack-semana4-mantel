//
//  MyViewCell.h
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface MyViewCell : UITableViewCell<UIAlertViewDelegate>

@property (weak, nonatomic) IBOutlet UILabel *myTitle;
@property (weak, nonatomic) IBOutlet UILabel *myDescription;
@property (weak, nonatomic) IBOutlet UIImageView *myPoster;
@property (weak, nonatomic) IBOutlet UIButton *myLike;

@property (strong, nonatomic) CoreDataManager * coreDataManager;
@property (strong, nonatomic) UserEntity * loggedUser;

@end
