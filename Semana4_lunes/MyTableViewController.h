//
//  MyTableViewController.h
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UserEntity.h"

@interface MyTableViewController : UITableViewController

@property (strong, nonatomic) CoreDataManager * coreDataManager;
@property (strong, nonatomic) UserEntity * loggedUser;

- (void) update;

@end
