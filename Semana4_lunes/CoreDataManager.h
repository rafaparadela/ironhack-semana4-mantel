//
//  CoreDataManager.h
//  Semana4_lunes
//
//  Created by Rafa Paradela on 24/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CoreDataManager : NSObject
@property (readonly, strong, nonatomic) NSManagedObjectContext *managedObjectContext;
- (instancetype)initWithModelName:(NSString *)modelName;
@end
