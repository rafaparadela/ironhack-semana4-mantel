//
//  BlockButtonItem.h
//  Semana4_lunes
//
//  Created by Rafa Paradela on 25/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^barButtonItemBlock)();

@interface BlockButtonItem : UIBarButtonItem

- (instancetype)initWithTitle: (NSString *) title block: (barButtonItemBlock)block;

@end
