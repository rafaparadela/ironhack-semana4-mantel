//
//  DetailViewController.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 25/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "DetailViewController.h"
#import "BlockButtonItem.h"

@interface DetailViewController ()



@end

@implementation DetailViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    BlockButtonItem * boton = [[BlockButtonItem alloc] initWithTitle:@"SI" block:^{
        NSLog(@"nooo");
    }];
    
    self.navigationItem.rightBarButtonItem = boton;
    
    
    //self.myTitle.text = self.serie.title;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
