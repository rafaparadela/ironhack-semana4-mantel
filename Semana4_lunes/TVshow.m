//
//  tvshows.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 23/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "TVshow.h"

@implementation TVshow


+ (NSDictionary *)JSONKeyPathsByPropertyKey {
    return @{
             @"id": @"id",
             @"title": @"title",
             @"description": @"description",
             @"posterURL": @"posterURL"
             };
}

@end
