//
//  BlockButtonItem.m
//  Semana4_lunes
//
//  Created by Rafa Paradela on 25/06/14.
//  Copyright (c) 2014 ironhack. All rights reserved.
//

#import "BlockButtonItem.h"

@interface BlockButtonItem ()

@property (copy, nonatomic) barButtonItemBlock block;

@end

@implementation BlockButtonItem

- (instancetype)initWithTitle: (NSString *) title block: (barButtonItemBlock)block
{
    self = [super initWithTitle:title style:UIBarButtonItemStylePlain target:self action:@selector(buttonAction:)];
    if (self) {
        _block = block;
        NSLog(@"siii");
    }
    return self;
}

- (void) buttonAction:(id)sender{
    self.block();
}

@end
